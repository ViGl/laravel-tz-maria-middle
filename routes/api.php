<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('comment/dd',      'CommentsApiController@dd');
Route::resource('comment',     'CommentsApiController');
Route::get('comment/{id}',    'CommentsApiController@show');
Route::get('comment/insert/{comment}',    'CommentsApiController@insertNewComment');
Route::get('comment/insertreview/{id}/{comment}',    'CommentsApiController@insertReviewComment');
Route::get('comment/edit/{id}/{comment}',    'CommentsApiController@edit');
Route::get('comment/delete/{id}/',    'CommentsApiController@delete');
