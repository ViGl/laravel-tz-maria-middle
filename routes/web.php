<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $url = "//".$_SERVER['HTTP_HOST'];
    return "
    <p>Доброго времени суток</p>
    <p> <a href='".$url."/posts'> Пользовательский интерфейс </a></p> 
    <p> <a href='".$url."/api/comment'> Просмотр JSON </a></p> 
    <p> /api/comment/insert/{Текст} - Добавить комментарий (родитель) </p> 
    <p> /api/comment/insertreview/{id}/{Текст} - Добавить ответ </p>      
    <p> /spi/comment/edit/{id} - Редактировать </p>   
    <p> /api/comment/delete/{id} - Удалить </p> 
    ";
});

Route::resource('posts',    'PostController');
Route::post('posts/ajax', 'PostController@ajaxView')->name('posts.ajaxView');

