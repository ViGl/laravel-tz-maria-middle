<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <p>Комментарии</p>
        @foreach($errors->all() as $error)
            <div style="color:red;">{{$error}}</div>
        @endforeach
        @if(\Session::has('success'))
            <div class="alert alert-success">
                {{\Session::get('success')}}
            </div>
        @endif
        <div class="row">
            <ul>
                @foreach($comments as $comment)
                    <li data-id="{{ $comment['id'] }}">
                        <form action="{{ route('posts.store') }}" method="post">
                            <label class="comment">{{$comment['comment']}}</label>
                            @if($comment['havingChild'])
                                <button class="view">Просмотреть ответы</button>
                            @endif
                            <input type="hidden" name="parent_id" value="{{ $comment['id'] }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="review">Ответить</button>
                            <button type="submit" name="edit" class="edit">Редактировать</button>
                            <button type="submit" name="delete" value="delete" class="delete">Удалить</button>
                        </form>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="row" style="padding:5px">
            <form method="post" action="{{ route('posts.store') }}">
                {{csrf_field()}}
                <textarea name="comment" id="" cols="30" rows="5"></textarea>
                <input type="submit" name="new_comment" value="Отправить">
            </form>
        </div>
    </body>
</html>
<script src="/js/jquery.js"></script>
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}

<script>
    $(function(){
        html = $('html');
        html.on('click', 'button.view', function(e){
            e.preventDefault();
            parent = $(this).parent();
            text_id = parent.find('input[name="parent_id"]').val();
            if(text_id) {
                $.post('{{ route('posts.ajaxView') }}', {id: text_id, _token: '{{ csrf_token() }}'}, function (data) {
                    html.find('li[data-id="' + text_id + '"]').append(data);
                });
                $(this).remove();
            }else{
                console.log('не могу найти');
            }
        });
        html.on('click', 'button.review', function(e){
            e.preventDefault();
            parent = $(this).parent();
            parent.append("<input type='text' name='comment'><button type='submit' name='send' value='send'>Отправить</button>");
            $(this).remove();
        })

        html.on('click', 'button.edit', function(e){
            e.preventDefault();

            parent = $(this).parent();
            text = parent.find('label.comment').text();
            $(this).remove();
            parent.find('label.comment').html('<input type="text" name="comment" value="'+text+'"><button type="submit" name="edit" value="send">Сохранить</button>');
            $(this).remove();
        })
    })

</script>