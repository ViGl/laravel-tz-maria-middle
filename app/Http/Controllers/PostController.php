<?php

namespace App\Http\Controllers;


use App\CommentsApi;
use App\Post;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Comment;

class PostController extends Controller
{

    public function getComments(){
        $request = CommentsApi::whereNull('parent_id')->get();
        foreach($request as $item){
            $havingChild = CommentsApi::where('parent_id', '=', $item->id)->first();
            $collection[$item->id] = [
                'id' => $item->id,
                'comment' => $item->comment,
                'havingChild' => $havingChild?1:0,
            ];
        }
        return $collection;
    }
    public function index(){
        $comments = $this->getComments();

        return view('welcome', [
            'comments' => $comments
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all(); //array
        if(isset($data['send'])){
            $this->ValidComment($request);
            $message = "Комментарий успешно добавлен!";
        }elseif(isset($data['new_comment'])){
            $this->ValidComment($request);
            $message = "Комментарий успешно добавлен!";
        }elseif(isset($data['edit'])){
            $data['id'] = $data['parent_id'];
            unset($data['parent_id']);
            $this->edit($data);
            $message = "Комментарий успешно отредактирован";
        }elseif(isset($data['delete'])){
            $data['id'] = $data['parent_id'];
            unset($data['parent_id']);
            $this->destroy($data['id']);
            $message = "Комментарий удален. Если он был деревом, то ветки тоже удалены!";
        }

        return redirect('/posts')->with('success', $message);
    }

    public function ValidComment($request){
        $this->validate($request,
            [ 'comment'=>'required' ],
            [ 'comment.required' => 'Пустой текст! Это не допустимо' ]);

        $data = $request->all();
        $post = new Post();
        $post->saveComment($data);
    }

    public function edit($item){
        $post = new Post;
        $post->updateComment($item);
    }

    public function destroy($id){
        $post = new Post;
        $post->deleteComment($id);
    }


    public function ajaxView(Request $request){
        $data = $request->all();
        $result = DB::table('posts')->where('parent_id', '=', $data['id'])->get();

        $html = "<ul>";
        foreach($result as $item):
            $have_child = DB::table('posts')->select('id')->where('parent_id', '=', $item->id)->first();

            $html .= "<form method='post' action='".route('posts.store')."'>";
                $html .= "<li data-id='".$item->id."'>";
                    $html .= '<label class="comment">'.$item->comment.'</label>';
                    if($have_child):
                        $html .= '<button class="view">Просмотреть ответы</button>';
                    endif;
                    $html .= '<input type="hidden" name="parent_id" value="'.$item->id.'">';
                    $html .= '<button class="review">Отправить</button>';
                    $html .= '<input type="hidden" name="_token" value="'.csrf_token().'">';
                    $html .= '<button type="submit" class="edit">Редактировать</button>';
                    $html .= '<button type="submit" class="delete">Удалить</button>';
                $html .= "</li>";
            $html .= "</form>";
        endforeach;
        $html .= "</ul>";

        return $html;
    }
}
