<?php

namespace App\Http\Controllers;

use App\CommentsApi;
use App\Http\Resources\CommentsApi as CommentsApiResource;
use App\Post;


class CommentsApiController extends Controller
{
    public function dd(){
        return dd(CommentsApi::all());
    }
    /**
     * @return CommentsApiResource
     */
    public function index(){
        return new CommentsApiResource(CommentsApi::all());
    }
    /**
     * @param $id
     * @return CommentsApiResource
     */
    public function show($id)
    {
        if($id){
            $post = CommentsApi::find($id);
            if($post){
                return new CommentsApiResource(CommentsApi::findOrFail($id));
            }else{
                dd('Такого итема не существуеТ!');
            }
        }
    }
    public function create()
    {
        dd($_POST);
    }
    public function insertNewComment($comment){
        $post =  new CommentsApi;
        $result =$post->insertComment($comment);
        return $result;
    }

    public function insertReviewComment($id ,$comment){
        if($id and $comment){
            $post = new CommentsApi;
            $result = $post->insertComment($comment, $id);
        }else{
            $result = "Ошибка";
        }
        return $result;
    }
    public function edit($id, $comment){
        if($id and $comment){
            $post = new CommentsApi;
            $post->edit($id, $comment);
        }else{
            dd('Ошибка');
        }
    }
    public function delete($id){
        $post = new CommentsApi;
        $post->destroyComment($id);
    }
}
