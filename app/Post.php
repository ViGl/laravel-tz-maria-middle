<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

//FOR CRUD
    public function saveComment($data){
        $this->parent_id = isset($data['parent_id'])?$data['parent_id']:null;
        $this->comment = $data['comment'];
        $this->save();
    }
    public function updateComment($data){
        $comment = $this->findOrFail($data['id']);
        $comment->comment = $data['comment'];
        $comment->save();
    }
    public function deleteComment($id){
        $this->where('id', '=', $id)->delete();
    }


}
