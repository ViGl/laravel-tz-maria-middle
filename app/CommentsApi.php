<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentsApi extends Model
{
    protected $table = 'posts';
    public function toArray()
    {
        return [
            'id' => $this->id,
            'comment' => $this->comment,
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }




    public function insertComment($comment, $id=null){
        //TODO Сделать првоерку на уникальность одинаковых сообщений. Может быть одинаковый текст в ветка но не в дереве!
        if(!empty($comment)) {
            if($id) {
                $finded = $this->find($id);
                if($finded) {
                    $this->parent_id = $id;
                }else{
                    dd("Такого родителя нет!");
                }
            }
            $this->comment = $comment;
            $this->save();
            return "Пост(".$this->id.") - Добавлен";
        }else{
            dd("Ошибка, пустой комментарий!");
        }
    }
    public function edit($id, $comment){
        $find = $this->find($id);
        if($find) {
            if($comment) {
                $find->comment = $comment;
                $find->save();
                dd('Отредактировали!');
            }else{
                dd('Пустой комментарий!');
            }
        }else{
            dd('Ошибка. Такого итема нет');
        }
    }
    public function destroyComment($id){
        if($id) {
            $find = $this->where('id', '=', $id)->first();
            if($find) {
                $this->where('id', '=', $id)->delete();
                dd("Удалил!");
            }else{
                dd("Такого итема не существует!!");
            }
        }else{
            dd("Пустой номер итема!");
        }

    }
}
